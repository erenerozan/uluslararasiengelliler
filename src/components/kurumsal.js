import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import "../react-tabs.css";

class Kurumsal extends Component {
  constructor(props){
    super(props);
     this.state={
       tabIndex:this.props.deneme.id
     };
     this.deger = this.deger.bind(this);
     console.log(this.props.deneme);
  }
  deger(sayi){
    let integ = parseInt(sayi,10);
    this.setState({
      tabIndex:integ
    });
    console.log(integ);
  }
  componentWillReceiveProps(newProps) {
    this.deger(newProps.deneme.id);
    console.log(newProps);
  }
  render() {
    const { tamveri } = this.props;
    function contextcreator(tag,veri,styles,i){
      var icerikveri = veri;
      switch (tag) {
        case "p":
          return (<p key={i} className={styles}>{icerikveri}</p>);

        case "li":
          return (<li key={i} className={styles}>{icerikveri}</li>);

        case "br":
          return (<br key={i}/>);

        case "div":
          return (<div key={i} className={styles}>{icerikveri}</div>);

        case "map":
          return(<iframe width="720" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps/ms?ie=UTF8&amp;hl=tr&amp;t=h&amp;msa=0&amp;msid=210991494361326851769.00049cdc896d8fc7aa3e8&amp;ll=37.035481,27.426467&amp;spn=0.015416,0.034289&amp;z=15&amp;output=embed"></iframe>);

        case "img":
          return (<img key={i} src={icerikveri} className={styles}/>);
        default:
          break;
      }
    }
    return (
      <div className="home">
        <Tabs selectedIndex={this.state.tabIndex} onSelect={index => this.deger(index)}>
          <TabList>
          {tamveri[0].linkler.map((x)=>{

               if(x.sidemenuVisible){
              return <Tab key={x.id}>{x.adi}</Tab>;
            }else{
              false;
            }
            })
          }

          </TabList>
          {tamveri.map((x)=>{
             return x.linkler.map((y)=>{
               if(y.sidemenuVisible){
              return <TabPanel key={y.id}>
              <h2>{y.adi}</h2>
              {y.icerikveri.icerikContext.map((icerikdata,i)=>{
                  var { tag, icerik, classname} = icerikdata;
                  var fonsiyondan = contextcreator(tag,icerik,classname,i);
                  return fonsiyondan;
                })
              }
              </TabPanel>;
            }else {
              false;
            }
            })
          })}

        </Tabs>
      </div>
    );
  }
}

export default Kurumsal;
