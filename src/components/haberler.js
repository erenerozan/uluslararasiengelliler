import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import '../react-tabs2.css';
import '../index.css';


class Haberler extends Component {
  constructor(props) {
    super(props)

  }

  render() {
    function contextcreator(tag,veri,styles,i){
      var icerikveri = veri;
      switch (tag) {
        case "p":
          return (<p key={i} className={styles}>{icerikveri}</p>);

        case "li":
          return (<li key={i} className={styles}>{icerikveri}</li>);

        case "br":
          return (<br key={i}/>);

        case "div":
          return (<div key={i} className={styles}>{icerikveri}</div>);

        case "map":
          return(<iframe width="720" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps/ms?ie=UTF8&amp;hl=tr&amp;t=h&amp;msa=0&amp;msid=210991494361326851769.00049cdc896d8fc7aa3e8&amp;ll=37.035481,27.426467&amp;spn=0.015416,0.034289&amp;z=15&amp;output=embed"></iframe>);

        case "img":
          return (<img key={i} src={icerikveri} className={styles}/>);
        default:
          break;
      }
    }
    const { deneme } = this.props;
    return (
      <div className="home">
        <div className="home-wrapper">
          <div className="baslik">
            {deneme.adi}
          </div>
          <hr/>
          <div className="aciklama">
          {deneme.icerikveri.icerikHeader}
          </div>
          {deneme.icerikveri.icerikContext.map((veri,i)=>{
            var { tag, icerik, classname} = veri;
            var fonsiyondan = contextcreator(tag,icerik,classname,i);
            return fonsiyondan;
          })}

        </div>

      </div>
    );
  }
}

export default Haberler;
