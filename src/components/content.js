import React, { Component } from 'react';
import { Switch, Route } from 'react-router';

import Home from './home.js';
import Haberler from './haberler.js';
import Kurumsal from './kurumsal.js';


class Content extends Component {
  componentDidMount(){
    //createRoute(componentData);
  }
  render() {
    const { componentData } = this.props;
    function compolar(veriler,propslar,tamveri) {
      switch (veriler) {
        case 0:
          var layout = (<Kurumsal deneme={propslar} tamveri={tamveri}/>);
          return layout;

        case 1:
          var layout1 = (<Home deneme={propslar} tamveri={tamveri}/>);
          return layout1;

        case 2:
          var layout2 = (<Haberler deneme={propslar} tamveri={tamveri}/>);
          return layout2;

        default:
          break;
      }
    }
    var obj =[];
    function createRoute(veri,tamveri){
      var zet= veri.map((x)=>{
        x.linkler.map((y)=>{
          if(y.subMenu){
            let layout=<Route
                 exact={y.exact}
                 path={y.paths}
                 key={y.id}
                 component={()=>(compolar(y.componenti,y,tamveri))}
             />;
             obj.push(layout);
             createRoute(y.subMenu);
           }else{
             let layout=<Route
                 exact={y.exact}
                 path={y.paths}
                 key={y.id}
                 component={()=>(compolar(y.componenti,y,tamveri))}
             />;
             obj.push(layout);

           }
        })
        return "";
        }
      )
      console.log(obj);
      return zet;

    }
    return (
      <div className="content">
        <Switch>
             { createRoute(componentData,componentData) }
            { obj.map((veri)=>veri) }
            {/*<Route exact path="/" component={Home}/>*/}
            {/*<Route path="/kurumsal/:id" component={Kurumsal}/>*/}
        </Switch>
      </div>
    );
  }
}

export default Content;
