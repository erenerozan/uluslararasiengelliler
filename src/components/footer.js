import React, { Component } from 'react';
import * as FontAwesome from 'react-icons/lib/fa'


class Footer extends Component {
  render() {
    return (
      <div className="footer">

        <div className="footer-first">
          <div className="footer-wrap">
              <a href="https://twitter.com/utefengelliler" target="_blank">
                <FontAwesome.FaTwitter size="24"/>
              </a>
              <a href="https://www.facebook.com/Uluslararasi-Engelliler-Ya%C5%9Flilar-Kimsesizler-Federasyonu-UTEF-1053763788013304/" target="_blank">
                <FontAwesome.FaFacebook size="24"/>
              </a>
              <a href="https://www.youtube.com/channel/UC47jsjw-klm4aciY83DyNyw?view_as=subscriber" target="_blank">
                <FontAwesome.FaYoutubePlay size="24"/>
              </a>
              <a href="https://www.instagram.com/utef.0/" target="_blank">
                <FontAwesome.FaInstagram size="24"/>
              </a>
              <a href="https://www.facebook.com/kalpantalya/  " target="_blank">
                <FontAwesome.FaNewspaperO size="24"/>
              </a>
              <a href="http://www.kalpantalya.com" target="_blank">
                <FontAwesome.FaPencil size="24"/>
              </a>
          </div>
        </div>
        <div className="footer-second">
          <div className="footer-wrap">
          Kuruluş Tarihi: 22.02.2010 Kütük No:07.035.075 Antalya./TÜRKİYE
          </div>
        </div>

      </div>
    );
  }
}

export default Footer;
