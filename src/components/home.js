import React, { Component } from 'react';
import Swiper from 'react-id-swiper';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Link } from 'react-router-dom';
import FacebookPlayer from 'react-facebook-player';
import * as FontAwesome from 'react-icons/lib/fa';

import '../swiper.css';
import '../react-tabs2.css';
import '../index.css';

const appId = '1597764217197632';
const videoIdA = '1656514671071543';


class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      player: null,
    };

    this.goNext = this.goNext.bind(this)
    this.goPrev = this.goPrev.bind(this)
    this.swiper = null
  }

  goNext() {
    if (this.swiper) this.swiper.slideNext()
  }

  goPrev() {
    if (this.swiper) this.swiper.slidePrev()
  }
  render() {
    const params = {
      pagination: '.swiper-pagination',
      paginationClickable: true,
      slidesPerView: 'auto',
      loop: true,
      effect: 'fade',
      autoplay: 5000,
      autoplayDisableOnInteraction: false,
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      spaceBetween: 30,
      runCallbacksOnInit: true,
      onInit: (swiper) => {
        this.swiper = swiper
      }
    };
    const { tamveri } = this.props;
    const { videoId, appId } = this.props;
    return (
      <div className="home">
        <Swiper {...params}>
          <div>
            <img src="http://www.utef.org/image/image1.jpg" alt=""/>
            <div>
              <div className="hiza">
              <div className="span">
                <div className="baslik">
                  Etkinlikler ile ilgili başlık
                </div>
                <div className="icerik">
                  Yaygın inancın tersine, Lorem Ipsum rastgele sözcüklerden oluşmaz. Kökleri M.Ö. 45 tarihinden bu yana klasik Latin edebiyatına kadar uzanan 2000 yıllık bir geçmişi vardır. Virginia'daki Hampden-Sydney College'dan Latince profesörü Richard McClintock, bir Lorem Ipsum pasajında geçen ve anlaşılması en güç sözcüklerden biri olan 'consectetur' sözcüğünün klasik edebiyattaki örneklerini incelediğinde kesin bir kaynağa ulaşmıştır.
                </div>
              </div>
              </div>
            </div>
          </div>

          <div>
            <img src="http://www.utef.org/image/image3.jpg" alt=""/>
            <div>
              <div className="hiza">
              <div className="span">
                <div className="baslik">
                  Başlık
                </div>
                <div className="icerik">
                  Yaygın inancın tersine, Lorem Ipsum rastgele sözcüklerden oluşmaz. Kökleri M.Ö. 45 tarihinden bu yana klasik Latin edebiyatına kadar uzanan 2000 yıllık bir geçmişi vardır. Virginia'daki Hampden-Sydney College'dan Latince profesörü Richard McClintock, bir Lorem Ipsum pasajında geçen ve anlaşılması en güç sözcüklerden biri olan 'consectetur' sözcüğünün klasik edebiyattaki örneklerini incelediğinde kesin bir kaynağa ulaşmıştır.
                </div>
              </div>
              </div>
            </div>
          </div>
        </Swiper>
        <Tabs className="react-tabs2">
          <TabList className="react-tabs2__tab-list2">
            <Tab className="react-tabs2__tab2">Son Haberler</Tab>
          </TabList>

          <TabPanel className="react-tabs2__tab-panel2">
          {tamveri[0].linkler[0].haberler.map((veri,i)=>{
            let { link, mesaj, widgetpic , baslik} = veri;
            if(link.stateless){
                let divstate = (<Link to={link.strings}>
                  <div className="haber-wrap">
                    <img src={widgetpic} alt="" />
                    <div className="baslik">
                      {baslik}
                    </div>
                    <div className="icerik">
                      {mesaj}
                    </div>
                  </div>
                </Link>);
              return divstate;
            }else{
              let divstate = (<a href={link.strings} target="_blank">
                <div className="haber-wrap">
                  <img src={widgetpic} alt="" />
                  <div className="baslik">
                    {baslik}
                  </div>
                  <div className="icerik">
                    {mesaj}
                  </div>
                </div>
              </a>);
              return divstate;
            }


          })}
          </TabPanel>

        </Tabs>
        <div className="services">
          <div className="services-in">
            <div className="left">
            <div className="baslik">
            Bizleri yanlız bırakmayan değerli DOSTLARIMIZA SONSUZ TEŞEKKÜRLER
            </div>
            <div className="icerik">
            UTEF Uluslararası Tüm Engelliler Yaşlılar kimsesizler federasyonu Bir Saz Bir Söz Bir Nefes THM 29 Ekim Cumhuriyet Bayramı kapsamında düzenlemiş olduğumuz Muhteşem Konserimizden.
            </div>
          </div>
          <div className="right">
            {/*<img src="http://www.utef.org/image/image1.jpg" alt="" />*/}
            <FacebookPlayer
              videoId={ videoIdA }
              appId={ appId }
              onReady={ this.onReady }
            />
          </div>
        </div>

        </div>
        <div className="services-light">
          <div className="services-in">
            <div className="left">
              <div className="baslik">
                2017 ENGELLİ VE YAŞLILAR PROJESİ
              </div>
              <div className="icerik">
                Uluslararası Keyifli Yaş Alma Ve Yaşama Şiir Şenliği
              </div>
            </div>
            <div className="right">
              <a href="http://www.utef.org/image/tuzuk.doc" download>
                <FontAwesome.FaDownload size="64"  color= '#006838'/>
                <span>İndir</span>
              </a>
              {/*<FacebookPlayer
                videoId={ videoIdA }
                appId={ appId }
                onReady={ this.onReady }
              />*/}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
