import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import DropdownMenu from 'react-dd-menu';

import Logo from './logo.png';
import Donation from './donation.png';


class Navbar extends Component {
  constructor(props){
    super(props);
    this.state={
      isMenuOpen: false,
      opens:[
        false,
        false
      ],
    };
    this.click = this.click.bind(this);
    this.toggle = this.toggle.bind(this);
    this.toggleout = this.toggle.bind(this);
    this.close = this.close.bind(this);
  }
  toggle(e) {
    console.log(e.target.title);
    let deger = e.target.title;
    const items = this.state.opens;
    items[deger] = true;

    this.setState({ items });
    console.log('toggle');
  }
  toggleout(e) {
    console.log(e.target.title);
    let deger = e.target.title;
    const items = this.state.opens;
    items[deger] = false;

    this.setState({ items });
    console.log('close');
  }
  close(e) {
    console.log(e.target);
    let deger = e.target.title;
    const items = this.state.opens;
    items[deger] = false;

    this.setState({ items });
    console.log('close');
  }

  click() {
    console.log('You clicked an item');
  }
  render() {

    var self= this;

    function ddmenu(data){
      console.log(data);
      console.log(data.linkler);
      var fat = data.map((veriler,i)=>{
          let menuOptions2 = {
            isOpen: self.state.opens[i],
            close: self.close,
            toggle: <div><div title={i} onMouseOver={self.toggle} className="menu-title">{veriler.baslik}</div><div className="menu-title"><Link to="/iletisim">İletişim</Link></div></div>,
            align: 'right'
          };
          return <div className="menus"  key={i}>
            <DropdownMenu {...menuOptions2} onMouseLeave={self.close}>
              <div title={i} onMouseLeave={self.close}>
              {veriler.linkler.map((icerik)=>{
                if(icerik.sidemenuVisible){
                  return (<li title={i} key={icerik.id} >
                    <Link title={i} to={icerik.paths}>
                      {icerik.adi}
                    </Link>
                  </li>
                )}else{
                  return false;
                }


              })}
              </div>
          </DropdownMenu>
          </div>;
          }
        );

      return fat;
    }
    return (
      <div className="navbar">

        <div className="menu" >
            <div className="menus-wrapper">
              <div className="logo">
                <Link to="/"><img src={Logo} alt=""/></Link>
                <div className="marka">Uluslararası Tüm Engelliler Yaşlılar Kimsesizler Federasyonu</div>
              </div>
              {ddmenu(this.props.data)}

              <div className="donation">
                <Link to="/bagis">
                  <img src={Donation} alt=""/>
                  <span>Bağış Yap</span>
                </Link>
              </div>
            </div>
        </div>
      </div>
    );
  }
}

export default Navbar;
