import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import Navbar from './components/navbar.js';
import Content from './components/content.js';
import Footer from './components/footer.js';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      data:[
        {
          id:0,
          baslik:"Kurumsal",
          linkler:[
            {
              id:110,
              paths:'/',
              adi:"Anasayfa",
              sidemenuVisible:false,
              exact:true,
              componenti:1,
              slider:[
                "http://clinicinternational.com.tr/2011/g/banner/mehmer_demircioglu.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/kadin_hastalik.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/banner-yeni-ilk.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/banner-yeni2.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/banner-yeni-3.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/memekanseri.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/bannerlar17.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/banner-15.jpg",
              ],
              widget:[
                {
                  id:"widget1",
                  link:"/hastahikayeleri",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"HASTA HİKAYELERİ",
                  widgetpic:"http://www.kadinhaberleri.com/images/haberler/kil_donmesi_kadinlarda_olur_mu_h600513.jpg"
                },
                {
                  id:"widget2",
                  link:"/iletisim",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"ACİL-İLETİŞİM-ULAŞIM",
                  widgetpic:"https://www.framestr.com/wp-content/uploads/2017/08/emergency-contact-form.jpg"
                },
                {
                  id:"widget3",
                  link:"/check_up",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"CHECK-UP",
                  widgetpic:"https://acibademmobil-wbeczwggocwcfpv3pwm.stackpathdns.com/wp-content/uploads/2017/01/bakicim-check-up-doktor-muayenesi.jpg"
                },
                {
                  id:"widget4",
                  link:"#",
                  classname:"widgetsecond",
                  classwrap:"widget-box2",
                  title:"VİDEO GALERİ",
                  widgetpic:"http://ahps.org.sg/wp-content/uploads/photo-gallery/41144817-HEALTH-word-cloud-concept-Stock-Photo-health.jpg"
                },
                {
                  id:"widget5",
                  link:"#",
                  classname:"widgetsecond",
                  classwrap:"widget-box2",
                  title:"SAĞLIKLI BİLGİLER",
                  widgetpic:"https://i.pinimg.com/736x/c4/db/c0/c4dbc0f1f6c23496f433ac82bc4cef83--healthy-tips-healthy-eating.jpg"
                },
              ],
              haberlerbaslik:"HABERLER ve DUYURULAR",
              haberler:[
                {
                  id:"widget1",
                  link:{
                    stateless:true,
                    strings:"/utef-genel-baskan-yardimcisi-guldane-kaya-ihtiyac-sahiplerini-yalniz-birakmadi"
                  },
                  baslik:"Utef genel başkan yardımcısı Güldane Kaya ihtiyaç sahiplerini yalnız bırakmadı.",
                  mesaj:`Uluslararası Tüm Engelliler Yaşlılar Kimsesizler Federasyonu (UTEF) Genel Başkan Yardımcısı ve Leman Gebizli Tüm Engellilere Umut Işığı Derneği Başkanı Güldane Kaya Kurban Bayramında ihtiyaç sahiplerinin yüzlerini güldürdü.`,
                  widgetpic:"http://www.antalyahabertakip.com/resimler/ana_48f4e141ce3f792f3ed0.jpg"
                },
                {
                  id:"widget2",
                  link:{
                    stateless:false,
                    strings:"http://www.haberlerankara.com/hayir-islerine-kendini-adamis-engelli-ve-yaslilara-yardim-eden-cesitli-kuruluslarda-y-3104yy.htm"
                  },
                  baslik:"Hayır işlerine kendini adamış, engelli ve yaşlılara yardım eden çeşitli kuruluşlarda yöneticilik yapan Güldane Kaya ile severek okuyacağınız bir söyleşi gerçekleştirdik",
                  mesaj:`Ben Isparta doğumluyum, 43 yaşındayım. Ispartayı pek bilmem hep Antalya’da yaşadım.`,
                  widgetpic:"http://www.haberlerankara.com/d/other/guldane-kaya-004.jpg"
                },
                {
                  id:"widget3",
                  link:{
                    stateless:false,
                    strings:"http://www.haberlerankara.com/utef-yorulmak-nedir-bilmiyor-3408yy.htm"
                  },
                  baslik:"UTEF YORULMAK NEDİR BİLMİYOR!",
                  mesaj:`Federasyon kurulalı yaklaşık on yıl olmuş. İhtiyaç sahiplerine eşya, gıda, giyim ( senin eskin bir başkasının yenisi kampanyası ile) sayısız insana ulaşmışlar. `,
                  widgetpic:"http://www.haberlerankara.com/d/other/utef-yorulmak-nedir-bilmiyor!-001.jpg"
                },
                {
                  id:"widget4",
                  link:{
                    stateless:false,
                    strings:"http://www.haberlerankara.com/anneler-gununu-coskuyla-kutladik-3283yy.htm"
                  },
                  baslik:"ANNELER GÜNÜNÜ COŞKUYLA KUTLADIK",
                  mesaj:`Leman Gebizli Tüm Engelli Yaşlı ve kimsesizlere Umut Işığı Derneği Başkanı Güldane Kaya Anneler Gününü Kutladı.`,
                  widgetpic:"http://www.haberlerankara.com/d/other/thumbnail_img_3627.jpg"
                },
              ],
              icerikveri:{
                icerikImage:"https://paratic.com/dosya/2015/11/kadinlari-cezbeden-birbirinden-cekici-araba-modelleri-bugatti-veyron.jpg",
                icerikHeader:"Deneme Başlığıdğr",
                icerikContext:"Yukardakinin aynısı"
              },
            },
            {
              id:0,
              paths:'/hakkimizda',
              adi:"Hakkımızda",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/image1.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"div",
                    icerik:"22.02.2010 Tarihinde kurulan Federasyonumuz ilk kuruluş aşamasında 5 Dernekle kurulmuştur.",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Her Derneğin insanlığa ayrı ayrı hizmetleri olmuş ve halen’de devam etmektedir.",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Federasyon olarak ta bizler yine aynı duygularla Sosyal sorumluluk Projeleri, Burslar, Aşevleri,",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Gıda Bankacılığı,  Kıyafet Bankacılığı, Eşya Bankacılığı,Tekerlekli Sandalye,Akülü araç hizmetlerini vermekteyiz.",
                    classname:"content-warning",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Çeşitli Korolar kurarak;Kültür ve sanata hizmet etmeyi, AB Projeleri ile Engelli ve Yaşlılarımıza sosyal yaşam alanları sağlamayı bir ilke edindik. Felsefemiz Halka hizmetin Hakka hizmet olduğudur.",
                    classname:"content-warning",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Allah Yar ve yardımcımız olsun…",
                    classname:"content-warning",
                  },
                ]
              },
            },
            {
              id:1,
              paths:'/misyon',
              adi:"Misyon",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/image3.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"div",
                    icerik:`İnsani ve ahlaki değerleri benimseyerek dil, din, ırk ve mezhep ayrımı yapmaksızın tüm engelli, yaşlı,
                    kimsesizlerin ihtiyaçlarını saptamak için gerekli çalışmaları yapmak ve toplum içerisinde dayanışmayı
                    sağlayarak gereken desteği yaratmaktır.`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },

                ]
              },
            },
            {
              id:2,
              paths:'/vizyon',
              adi:"Vizyon",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/image1.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"div",
                    icerik:`Sosyal ve Kültürel projeler ve etkinlikler ile yardıma muhtaç tüm kişilere ulaşabilmek, onlara ve
                    ailelerine en iyi desteği sağlamak ve güvenilir, bilinen bir sivil toplum kuruluşu olmak.`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:3,
              paths:'/amac',
              adi:"Amaç",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/image3.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"div",
                    icerik:`Toplumun dikkatini, ilgisini ve özenini engellilerin birey olmalarındaki mücadeleye çekmek ve yeni
                    üyelerin bu oluşumunda etkin olarak yer almasını sağlamak,`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`Sağlık, eğitim ve yaşam kalitesi konularında engelli, yaşlı, kimsesizlerin sürekli gelişmelerini sağlamak,`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`İhtiyaç sahiplerinin ihtiyaçlarını temin ile hayatlarını devam ettirmelerinde rehber ve destek olmak.`,
                    classname:"content-warning",
                  },
                ]
              },
            },
            {
              id:4,
              paths:'/degerler',
              adi:"Değerlerimiz",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/image1.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"div",
                    icerik:`Yardıma muhtaç kişilerin hak ve taleplerinin savunucusu, aynı zamanda takipçisi olmak, onların
                    rehabilitasyonlarını sağlamak, onlar için gereken araç gereçleri temin etmek, maddi ve manevi
                    yardımda bulunarak onlara ve ailelerine destek olmak öncelikli değerimizdir.`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:5,
              paths:'/bagis',
              adi:"Bağış Yolları",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/image1.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"div",
                    icerik:`T.C ZİRAAT BANKASI Şube 1236.Üç Kapılar Antalya Şubesi`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`IBAN No: TR800001001236834463445001     TL.    Hesabı`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`IBAN No: TR530001001236834463445002     €        Hesabı`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`IBAN No: TR260001001236834463445003     $        Hesabı`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:6,
              paths:'/temsilcilik',
              adi:"Temsilciliklerimiz",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/image1.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"div",
                    icerik:`1-Ankara İl Temsilcimiz Alisa Çiçek Akyol`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`2-İstanbul Anadolu Yakası İl Temsilcimiz Mehmet Nejat Kansu`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`3-Uşak İl Temsilcimiz Senem Özpınar`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`4-Burdur İl Temsilcimiz Hasan Hüseyin İçli`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`5-Konya İl Temsilcimiz Erol Mintaş`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`6-Afyon İl Temsilcimiz Refik Akdağ`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`7-Ege Bölge Başkanı ve İzmir İl Başkanı Ömer Sabri Kurşun`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:7,
              paths:'/baglidernekler',
              adi:"Federasyona Bağlı Dernekler",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/image1.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"div",
                    icerik:`1- Nasreddin Hoca Tüm Engelliler Yaşlılar Kimsesizler Derneği`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`2- Yeryüzü Meleklerini Koruma Derneği`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`3- Kalp Gözü Tüm Engelli Yaşlı Ve Kimsesiz Sanatçılar Derneği`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`4- Lösemili Ve Engelli Çocuklara Yardım Derneği`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`5- Uşak Engel Tanımayanlar Derneği`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`6- Leman Gebizli Tüm Engelli Yaşlı Ve Kimsesizlere Umut Işığı Derneği`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:21,
              paths:'/utef-genel-baskan-yardimcisi-guldane-kaya-ihtiyac-sahiplerini-yalniz-birakmadi',
              adi:`Utef genel başkan yardımcısı Güldane Kaya ihtiyaç sahiplerini yalnız bırakmadı.`,
              sidemenuVisible:false,
              exact:false,
              componenti:2,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:`Uluslararası Tüm Engelliler Yaşlılar Kimsesizler Federasyonu (UTEF) Genel Başkan Yardımcısı ve Leman Gebizli Tüm Engellilere Umut Işığı Derneği Başkanı Güldane Kaya Kurban Bayramında ihtiyaç sahiplerinin yüzlerini güldürdü.`,
                icerikContext:[
                  {
                    tag:"img",
                    icerik:"http://www.antalyahabertakip.com/resimler/ana_48f4e141ce3f792f3ed0.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"div",
                    icerik:`UTEF Genel Başkan Yardımcısı Güldane Kaya yaptığı açıklamada şu ifadeleri kullandı “ Kurban Bayramı münasebetiyle hayırseverlerimizin bizleri arayıp kurban etlerini ihtiyaç sahibi Aile'lerimizle paylaşmak istediklerini belirtip bizlerden destek istediler.`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`Bizlerde seve seve bu kutsal görevi kabul edip yerine getirdik ve ihtiyaç sahibi olan Aile'lerimizin etlerini yerlerine ulaştırdık. Bayram'ın bu son günü dolayısıyla Sabah 10:00 dağıtıma başladık güzel ailelerle bayramlaşıp paketlerini teslim edip hayır dualarını aldık.`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`Tüm Hayır Sever Bağışçılarımıza çok teşekkür ederiz.
                    Allah razı olsun Allah kurbanlarını kabul etsin.
                    Bu Güzel görevi Uluslararası Tüm Engelliler Yaşlılar Kimsesizler Federasyonu (UTEF) Genel Başkan'ım M.Mete Kaçar ile birlikte yaptık.
                    Ayrıca Uluslararası Tüm Engelliler Yaşlılar Kimsesizler Federasyonu (UTEF) Genel Başkan’ımızda teşekkür ediyorum işlerini bırakarak dağıtım esnasında bana eşlik ettiği için” dedi.`,
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:23,
              paths:'/hayir-islerine-kendini-adamis-engelli-ve-yaslilara-yardim-eden-cesitli-kuruluslarda',
              adi:`Hayır işlerine kendini adamış, engelli ve yaşlılara yardım eden çeşitli kuruluşlarda yöneticilik yapan Güldane Kaya ile severek okuyacağınız bir söyleşi gerçekleştirdik`,
              sidemenuVisible:false,
              exact:false,
              componenti:2,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:`Hayır işlerine kendini adamış, engelli ve yaşlılara yardım eden çeşitli kuruluşlarda yöneticilik yapan Güldane Kaya ile severek okuyacağınız bir söyleşi gerçekleştirdik`,
                icerikContext:[
                  {
                    tag:"img",
                    icerik:"http://www.haberlerankara.com/d/other/guldane-kaya-004.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"div",
                    icerik:`UTEF Genel Başkan Yardımcısı Güldane Kaya yaptığı açıklamada şu ifadeleri kullandı “ Kurban Bayramı münasebetiyle hayırseverlerimizin bizleri arayıp kurban etlerini ihtiyaç sahibi Aile'lerimizle paylaşmak istediklerini belirtip bizlerden destek istediler.`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`Bizlerde seve seve bu kutsal görevi kabul edip yerine getirdik ve ihtiyaç sahibi olan Aile'lerimizin etlerini yerlerine ulaştırdık. Bayram'ın bu son günü dolayısıyla Sabah 10:00 dağıtıma başladık güzel ailelerle bayramlaşıp paketlerini teslim edip hayır dualarını aldık.`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`Tüm Hayır Sever Bağışçılarımıza çok teşekkür ederiz.
                    Allah razı olsun Allah kurbanlarını kabul etsin.
                    Bu Güzel görevi Uluslararası Tüm Engelliler Yaşlılar Kimsesizler Federasyonu (UTEF) Genel Başkan'ım M.Mete Kaçar ile birlikte yaptık.
                    Ayrıca Uluslararası Tüm Engelliler Yaşlılar Kimsesizler Federasyonu (UTEF) Genel Başkan’ımızda teşekkür ediyorum işlerini bırakarak dağıtım esnasında bana eşlik ettiği için” dedi.`,
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:8,
              paths:'/medya',
              adi:"Medya",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/1.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/2.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/3.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/4.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/5.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/6.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/7.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/8.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/9.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/10.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/11.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/12.jpg",
                    classname:"content-img",
                  },
                ]
              },
            },
            {
              id:9,
              paths:'/iletisim',
              adi:"İletişim",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"img",
                    icerik:"http://www.utef.org/image/image1.jpg",
                    classname:"content-img",
                  },
                  {
                    tag:"div",
                    icerik:`ADRES: Tuzcular Mah. İmaret Sok.Faraçlar Pasajı No:11 Muratpaşa/ANTALYA-TÜRKİYE`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`Tel:+90.0242.240.1000 Gsm:+90.532.065.8898`,
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                ]
              },
            },

          ]
        },
        /*{
          id:1,
          baslik:"Çalışma Alanları",
          linkler:[
            {
              id:1515,
              paths:'/bagis',
              adi:"ikincikisim",
              sidemenuVisible:false,
              exact:true,
              componenti:1,
              slider:[
                "http://clinicinternational.com.tr/2011/g/banner/mehmer_demircioglu.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/kadin_hastalik.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/banner-yeni-ilk.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/banner-yeni2.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/banner-yeni-3.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/memekanseri.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/bannerlar17.jpg",
                "http://clinicinternational.com.tr/2011/g/banner/banner-15.jpg",
              ],
              widget:[
                {
                  id:"widget1",
                  link:"/hastahikayeleri",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"HASTA HİKAYELERİ",
                  widgetpic:"http://www.kadinhaberleri.com/images/haberler/kil_donmesi_kadinlarda_olur_mu_h600513.jpg"
                },
                {
                  id:"widget2",
                  link:"/iletisim",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"ACİL-İLETİŞİM-ULAŞIM",
                  widgetpic:"https://www.framestr.com/wp-content/uploads/2017/08/emergency-contact-form.jpg"
                },
                {
                  id:"widget3",
                  link:"/check_up",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"CHECK-UP",
                  widgetpic:"https://acibademmobil-wbeczwggocwcfpv3pwm.stackpathdns.com/wp-content/uploads/2017/01/bakicim-check-up-doktor-muayenesi.jpg"
                },
                {
                  id:"widget4",
                  link:"#",
                  classname:"widgetsecond",
                  classwrap:"widget-box2",
                  title:"VİDEO GALERİ",
                  widgetpic:"http://ahps.org.sg/wp-content/uploads/photo-gallery/41144817-HEALTH-word-cloud-concept-Stock-Photo-health.jpg"
                },
                {
                  id:"widget5",
                  link:"#",
                  classname:"widgetsecond",
                  classwrap:"widget-box2",
                  title:"SAĞLIKLI BİLGİLER",
                  widgetpic:"https://i.pinimg.com/736x/c4/db/c0/c4dbc0f1f6c23496f433ac82bc4cef83--healthy-tips-healthy-eating.jpg"
                },
              ],
              haberlerbaslik:"HABERLER ve DUYURULAR",
              haberler:[
                {
                  id:"widget1",
                  link:"/hakkimizda",
                  mesaj:`6 yıldır Bodrum'da başka sağlık kuruluşlarında çalışan Kardiyoloji ( Kalp Hastalıkları ) Uzmanı Doktor Fikret Mert Acar kurumumuzda SGK anlaşmalı olarak çalışmaya başlamıştır, kendisine başarılar diliyoruz.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
                {
                  id:"widget2",
                  link:"/hakkimizda",
                  mesaj:`5 Yıldır Bodrum'da başka bir sağlık kuruluşunda çalışan Kadın Hastalıkları ve Doğum Uzmanı Op.Dr. Ömer Orçun Koçak merkezimizde SGK anlaşmalı olarak çalışmaya başlamıştır, kendisine başarılar diliyoruz.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
                {
                  id:"widget3",
                  link:"/hakkimizda",
                  mesaj:`Genel Cerrahi Uzmanı Doçent Doktor Cengiz Kayahan SGK anlaşmalı olarak hasta kabulüne başlamıştır kendisine başarılar diliyoruz.Hekimimiz özellikle obezite cerrahisi (mide küçültme ameliyatları ) , tiroid cerrahisi ( guatr ameliyatları ) bağırsak ameliyatları, ve meme cerrahisi konusunda üstün deneyime sahiptir.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
                {
                  id:"widget4",
                  link:"/hakkimizda",
                  mesaj:`Kulak Burun Boğaz Uzmanı Op. Dr. Lemi Özer merkezimizde SGK anlaşmalı olarak çalışmaya başlamıştır. 3 yıl boyunca Acıbadem Bodrum Hastanesi'nde görev yapan hekim, başarılı meslek hayatına tıp merkezimizde devam edecektir.  Kendisine başarılar diliyoruz.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
              ],
              icerikveri:{
                icerikImage:"https://paratic.com/dosya/2015/11/kadinlari-cezbeden-birbirinden-cekici-araba-modelleri-bugatti-veyron.jpg",
                icerikHeader:"Deneme Başlığıdğr",
                icerikContext:"Yukardakinin aynısı"
              },
            },
            {
              id:6,
              paths:'/hakkimizda2',
              adi:"Hakkımızda2",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"div",
                    icerik:"Turizm ve eğlence cenneti Bodrum, her yıl tatil hayallerini gerçekleştirmek için dünyanın dört bir yanından gelen binlerce misafir ağırlıyor. Ancak kimi zaman küçük bir rahatsızlık, ya da umulmadık bir kaza- bir hastalık, tüm planları ve tüm tatil hayallerini alt-üst edebiliyor.",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Kaza,hastalık ve yaralanmalar gibi beklenmedik durumlar ne zaman başınıza gelse, üzüntü, endişe ve sıkıntı kaynağı olurlar; üstelikte bilmediğiniz bir ülkede- şehirde- kültürde iken, hastalanmak bu korku ve endişeleri daha çok arttırır.",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Bizler bu korkularınızı çok iyi anlıyor, ve sizler için huzurlu, güvenli ve rahat bir ortam yaratmaya çabalıyoruz. Butik klinik konsepti ile Özel Clinic International BMC Tıp Merkezi, güleryüzlü ve modern hizmet anlayışıyla, sizlerin sorunlarına kısa sürede çözüm bulup, iyileşmenize yardımcı olurken, evinizdeki konfor ve huzuru da aratmamak için 7 gün 24 saat hizmetinizdedir.",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Özel Clinic International Tıp Merkezi, uzman ve seçkin kadrosu, güleryüzlü, modern hizmet anlayışıyla, hasta hakları ve etik kurallara saygılı, tıp, teknoloji ve sağlık sektöründeki gelişmeleri takip ederek, kendi hizmet kalitesini sürekli yenileyerek arttıran, hızıl ve kesin tedavi yaklaşımıyla koruyucu ve iyileştirici sağlık hizmetleri sunan butik bir kliniktir.",
                    classname:"content-warning",
                  },
                ]
              },
            },
            {
              id:9,
              paths:'/zaman',
              adi:"Zaman",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"div",
                    icerik:"Turizm ve eğlence cenneti Bodrum, her yıl tatil hayallerini gerçekleştirmek için dünyanın dört bir yanından gelen binlerce misafir ağırlıyor. Ancak kimi zaman küçük bir rahatsızlık, ya da umulmadık bir kaza- bir hastalık, tüm planları ve tüm tatil hayallerini alt-üst edebiliyor.",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Kaza,hastalık ve yaralanmalar gibi beklenmedik durumlar ne zaman başınıza gelse, üzüntü, endişe ve sıkıntı kaynağı olurlar; üstelikte bilmediğiniz bir ülkede- şehirde- kültürde iken, hastalanmak bu korku ve endişeleri daha çok arttırır.",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Bizler bu korkularınızı çok iyi anlıyor, ve sizler için huzurlu, güvenli ve rahat bir ortam yaratmaya çabalıyoruz. Butik klinik konsepti ile Özel Clinic International BMC Tıp Merkezi, güleryüzlü ve modern hizmet anlayışıyla, sizlerin sorunlarına kısa sürede çözüm bulup, iyileşmenize yardımcı olurken, evinizdeki konfor ve huzuru da aratmamak için 7 gün 24 saat hizmetinizdedir.",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Özel Clinic International Tıp Merkezi, uzman ve seçkin kadrosu, güleryüzlü, modern hizmet anlayışıyla, hasta hakları ve etik kurallara saygılı, tıp, teknoloji ve sağlık sektöründeki gelişmeleri takip ederek, kendi hizmet kalitesini sürekli yenileyerek arttıran, hızıl ve kesin tedavi yaklaşımıyla koruyucu ve iyileştirici sağlık hizmetleri sunan butik bir kliniktir.",
                    classname:"content-warning",
                  },
                ]
              },
            },
          ]
        }
        */
     ]
    }

  }
  render() {
    const Wrapper = ()=>(
      <div>
      <Navbar data={this.state.data}/>
      <Content componentData={this.state.data}/>
      <Footer/>
      </div>
    );
    return (
      <BrowserRouter>
      <Wrapper/>
      </BrowserRouter>
    );
  }
}

export default App;
